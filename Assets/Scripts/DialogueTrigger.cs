using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Flowchart flowchart02;
    public Flowchart flowchart03;
    private bool isOpen = false;
    private bool isClosed = true;

    private void OnTriggerEnter(Collider other)
    {

        if (isOpen == true)
        {
            return;
        }
        else if (other.gameObject.tag == "XULT" && isClosed == true)
        {
            isClosed = false;
            flowchart03.ExecuteBlock("SomethingElse");
            Debug.Log("Dom");
            isOpen = true;
        }
        else if (other.gameObject.tag == "RT" && isClosed == true)
        {
            isClosed = false;
            flowchart02.ExecuteBlock("New Block");
            Debug.Log("Flowchart2");
            isOpen = true;
        }
        else
        {
            isOpen = false;
        }

    }

}
