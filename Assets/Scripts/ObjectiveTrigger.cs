using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveTrigger : MonoBehaviour
{
    private bool isLookingAtNotes = false;
    public GameObject rationalesNotes;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            isLookingAtNotes = !isLookingAtNotes;
            if(isLookingAtNotes)
            {
                rationalesNotes.SetActive(true);
            }
            else
            {
                rationalesNotes.SetActive(false);
            }

        }
    }
}
