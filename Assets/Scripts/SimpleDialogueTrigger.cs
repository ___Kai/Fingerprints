using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDialogueTrigger : MonoBehaviour
{
    public Flowchart flowchart;
    private bool isOpen = false;
    private void OnTriggerEnter(Collider misc)
    {
        if (isOpen == true)
        {

            return;
        }
        else
        {
            flowchart.ExecuteBlock("Extra");
            isOpen = true;
            Debug.Log("Well, you'd certainly hope that'd work, wouldn't you?");
        }
    }
}
