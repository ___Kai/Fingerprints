using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionScreen : MonoBehaviour
{
    public GameObject instructionScreen;
    public GameObject CreditScreen;
    public Animator animator;
    public void OpenInstructions()
    {
        instructionScreen.SetActive(true);
    }
    public void CloseInstructions()
    {
        instructionScreen.SetActive(false);
    }
    public void OpenCredits()
    {
        CreditScreen.SetActive(true);
        animator.SetBool("IsOpen", true);
    }
    public void CloseCredits()
    {
        CreditScreen.SetActive(false);
        animator.SetBool("IsOpen", false);
    }
}
