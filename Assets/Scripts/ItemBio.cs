using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBio : MonoBehaviour
{
    [TextArea(3, 10)]
    public string bio;
}
