using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pickUpItem : MonoBehaviour
{
    [SerializeField] private float radius;
    private bool isHolding = false;
    private bool isInterestedInLore = false;
    public Camera fpsCamera;
    public GameObject hud;
    public GameObject bioUIElement;
    public GameObject pickupableObject;
    public Text hudInstructionText;
    public Text bioUIText;
    public int objectLayer;

    // Start is called before the first frame update
    void Start()
    {
        objectLayer = 1 << LayerMask.NameToLayer("Object");
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.CheckSphere(transform.position, radius, objectLayer))
        {
            Collider[] gsd = Physics.OverlapSphere(transform.position, radius, objectLayer);
            //Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out Hitinfo, range);
            hud.SetActive(true);
            if (isHolding == false)
            {
                //hudInstructionText.text = "Press [E] to pick up \n" + gsd[0].transform.name;
                hudInstructionText.text = "Press [E] to pick up \n" + gsd[0].GetComponent<DisplayName>().displayName + "\n Press [TAB] to read Item Bio.";
            }
            else
            {
                hudInstructionText.text = "Press [E] to drop \n" + gsd[0].GetComponent<DisplayName>().displayName + "\n Press [TAB] to read Item Bio.";

            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("hELLO WORLD");
                isHolding = !isHolding;
                if(isHolding == true)
                {
                    SetParent(gsd[0].gameObject);
                }
                else
                {
                    DetachFromParent(gsd[0].gameObject);
                }
            }
            if(Input.GetKeyDown(KeyCode.Tab))
            {
                isInterestedInLore = !isInterestedInLore;
                if(isInterestedInLore)
                {
                    bioUIElement.SetActive(true);
                    bioUIText.text = gsd[0].GetComponent<ItemBio>().bio;
                }
                else
                {
                    bioUIElement.SetActive(false);
                }
            }
        }
        else
        {
            hud.SetActive(false);
        }
    }
    void SetParent(GameObject newParent)
    {
        //pickupableObject.transform.parent = newParent.transform;
        newParent.transform.parent = transform;
    }
    void DetachFromParent(GameObject newParent)
    {
        newParent.transform.parent = null;
    }
}
